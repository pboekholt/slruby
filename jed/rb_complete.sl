require("slruby");
rb_load_file(path_dirname(__FILE__) + "/rb_complete.rb");
define rb_complete()
{
   push_spot_bol();
   push_mark();
   pop_spot();
   variable input = bufsubstr();
   variable completion  = rb_call_function("get_completions", input);
   if (completion != NULL)
     insert(completion);
}

define rb_tab()
{
   variable col = what_column();
   indent_line();
   if (col == what_column())
     rb_complete();
}

