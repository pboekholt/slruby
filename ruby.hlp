rb_eval

 SYNOPSIS
  evaluate Ruby code

 USAGE
  rb_eval(String s)

 DESCRIPTION
  Evaluate `s' as Ruby code and return its result to S-Lang.

 SEE ALSO
  rb_call_function, rb_call_method, rb_load_file

--------------------------------------------------------------

rb_call_function

 SYNOPSIS
  call a Ruby function

 USAGE
  rb_call_function(s, params)

 DESCRIPTION
  Call the Ruby function `s' with parameters `params' and return its
  result to S-Lang.

 NOTES
  If the last parameter is a reference, it is converted to a Ruby block.

 SEE ALSO
  rb_eval, rb_call_method, rb_load_file

--------------------------------------------------------------

rb_call_method

 SYNOPSIS
  Call a method on a Ruby object

 USAGE
  rb_call_method(v, String s, params)

 DESCRIPTION
  Call the Ruby method `s' on `v' with parameters `params' and
  return its result to S-Lang

 NOTES
  If the last parameter is a reference, it is converted to a Ruby block.
  `v' has to be a Ruby_Value_Type, or a type that can be passed to
  Ruby. See the README for more on converting types between S-Lang and
  Ruby. If `v' is a Ruby_Value_Type, you can also use the notation
  `v.s(params)'.

 SEE ALSO
  rb_eval, rb_call_function, rb_load_file

--------------------------------------------------------------

rb_load_file

 SYNOPSIS
  Load a Ruby file

 USAGE
  rb_load_file(String s)

 DESCRIPTION
  Loads the Ruby file with filename `s' into Ruby

 SEE ALSO
  rb_eval, rb_call_function, rb_call_method

--------------------------------------------------------------
