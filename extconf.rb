# Makefile generator for the S-Lang Ruby module
require 'mkmf'

# This is needed to get a static Ruby library linked
$LIBS = "#$LIBRUBYARG_STATIC #$LIBS" if  CONFIG['LIBRUBY'] == '$(LIBRUBY_A)'

# For Ruby 1.9
have_type('rb_block_call_func', 'ruby.h')

# If ruby_init_stack() exists, use it.
have_func('ruby_init_stack', 'ruby.h')

# Find the slang library

if find_library("slang", "", "/usr/local/lib")
  slang_prefix="/usr/local"
elsif find_library("slang", "", "/usr/lib")
  slang_prefix="/usr"
else
  puts "could not find slang library"
  exit
end

# Get the Makefile to install to /usr/local/lib/slang/v2/modules

CONFIG['sitedir']=slang_prefix + '/lib/slang'
RbConfig::CONFIG['ruby_version']="v2"
CONFIG['sitearch']="modules"

dir_config("slang", slang_prefix)

$INSTALLFILES=[["*.sl", slang_prefix + "/share/slsh/local-packages"],
["*.hlp", slang_prefix + "/share/slsh/local-packages/help"]]
create_makefile 'ruby-module'

# "make clean" should not remove .sl files
puts "fixing Makefile"
IO.popen("ed Makefile", "w") { |f| f << %q{/\*\.s\[ol\]/s///
wq
}
}
