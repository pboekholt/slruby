\function{rb_eval}
\synopsis{evaluate Ruby code}
\usage{rb_eval(String s)}
\description
  Evaluate \exmp{s} as Ruby code and return its result to \slang.
\seealso{rb_call_function, rb_call_method, rb_load_file}
\done

\function{rb_call_function}
\synopsis{call a Ruby function}
\usage{rb_call_function(s, params)}
\description
  Call the Ruby function \exmp{s} with parameters \exmp{params} and return its
  result to \slang.
\notes
  If the last parameter is a reference, it is converted to a Ruby block.
\seealso{rb_eval, rb_call_method, rb_load_file}
\done

\function{rb_call_method}
\synopsis{Call a method on a Ruby object}
\usage{rb_call_method(v, String s, params)}
\description
  Call the Ruby method \exmp{s} on \exmp{v} with parameters \exmp{params} and
  return its result to \slang
\notes
  If the last parameter is a reference, it is converted to a Ruby block.
  \exmp{v} has to be a Ruby_Value_Type, or a type that can be passed to
  Ruby. See the README for more on converting types between \slang and
  Ruby. If \exmp{v} is a Ruby_Value_Type, you can also use the notation
  \exmp{v.s(params)}.
\seealso{rb_eval, rb_call_function, rb_load_file}
\done

\function{rb_load_file}
\synopsis{Load a Ruby file}
\usage{rb_load_file(String s)}
\description
  Loads the Ruby file with filename \exmp{s} into Ruby
\seealso{rb_eval, rb_call_function, rb_call_method}
\done
