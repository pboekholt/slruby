#<ruby>
def concat(a,b)
  return a + b
end
class TestClass
  attr_accessor :foo
  def initialize
    @foo = "bar"
  end
  def testmethod(*args)
    if block_given?
      return args.map{|a| yield a}.join
    end
    return args.join
  end
  def test_sget(*args)
    return *args
  end
end
__END__
#</ruby>
require("slruby");
rb_load_file(__FILE__);
define test_eval()
{
   rb_eval("1+1");
}
variable test_variable = 3;
variable test_long_long=1LL;
variable test_ulong_long=1ULL;
define get_test_variable()
{
   test_variable;
}

define myblock(a)
{
   return strup(a);
}
private  variable t = rb_eval("TestClass.new");

define test_sget()
{
   variable e;
   try (e)
     {
	t.test_sget(1,2,3,4);
     }
   catch AnyError:
     {
	print(e);
     }
}

define test_sget_block()
{
   variable e;
   try (e)
     {
	t.testmethod("foo", "bar", &myblock);
     }
   catch AnyError:
     {
	print(e);
     }
}

define test_sput()
{
   variable e;
   try (e)
     {
	t.foo = "baz";
	t.foo();
     }
   catch AnyError:
     {
	print(e);
     }
}

define test_call_function()
{
   variable e;
   try (e)
     {
	rb_call_function("concat", "foo", "bar");
     }
   catch AnyError:
     {
	print(e);
     }
}

define test_call_method()
{
   variable args = __pop_list(_NARGS);
   ifnot (length(args))
     args ={"foo"};
   variable e;
   try (e)
     {
	rb_call_method(t, "testmethod", __push_list(args));
     }
   catch AnyError:
     {
	print(e);
     }
}

define test_call_method_block()
{
   variable e;
   try (e)
     {
	rb_call_method(t, "testmethod","foo", "bar", &myblock);
     }
   catch AnyError:
     {
	print(e);
     }
}

define test_hash(h)
{
   h["foo"] = 1;
   h["bar"] = 2;
   h, h["bar"], h.length();
}

define test_conversion(v)
{
   return v;
}

rb_load_file(path_concat(path_dirname(__FILE__), "tests.rb"));
