require "test/unit"
require "test/unit/ui/console/testrunner"
class TestSlang < Test::Unit::TestCase
  def test_eval
    assert_equal([2], SLang.test_eval)
  end
  def test_variable
    assert_equal(3, SLang.test_variable)
    SLang.test_variable = 4
    assert_equal(4, SLang.test_variable)
    SLang.test_variable += 1
    assert_equal([5], SLang.get_test_variable)
    assert_equal(1, SLang.test_long_long)
    assert_equal(1, SLang.test_ulong_long)
  end
  
  # passing parameters from Ruby to S-Lang
  def test_array
    assert_equal([3], SLang.length([1,2,3]))
    assert_equal(["foobar"], SLang.strcat("foo", "bar"))
    assert_equal(["foobar"], SLang.strcat(:foo, :bar))
    assert_equal([4], SLang.sqr(-2))
    # 2 ** 32 = 4294967296.0
    assert_equal([2 ** 32], SLang.test_conversion(2 ** 32))
    assert_equal([0.25], SLang.sqr(0.5))
    assert_equal([1], SLang.list_pop([1,2,3]))
    assert_equal([/foo/], SLang.test_conversion(/foo/))
  end
  
  # sget function
  def test_sget
    assert_equal([[1,2,3,4]], SLang.test_sget)
    assert_equal(["FOOBAR"],  SLang.test_sget_block)
  end
  
  def test_sput
    assert_equal(["baz"], SLang.test_sput)
  end
  
  # rb_call_function
  def test_call_function
    assert_equal(["foobar"], SLang.test_call_function)
  end
  
  # rb_call_method
  def test_call_method
    assert_equal(["foo"], SLang.test_call_method)
    assert_equal(["foo"], SLang.test_call_method("foo"))
    assert_equal(["foobar"], SLang.test_call_method("foo", "bar"))
    assert_equal(["FOOBAR"],  SLang.test_call_method_block)
  end
  
  # hashes
  def test_hash
    assert_equal([{"foo" => 1, "bar" => 2}, 2, 2], SLang.test_hash({"foo" => nil}))
  end
end
Test::Unit::UI::Console::TestRunner.run(TestSlang)
